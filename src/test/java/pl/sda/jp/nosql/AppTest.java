package pl.sda.jp.nosql;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    public void getFilteredDocuments() {
        MongoCollection<Document> books = getDatabase().getCollection("books");

        BasicDBObject filterObject = new BasicDBObject();
        filterObject.put("title", "Java");


        MongoCursor<Document> mongoCursor = books.find()
//                .filter(filterObject)
//                .filter(BasicDBObject.parse("{title: \"Java\"}"))
                .filter(Filters.eq("title", "Java"))
                .sort(BasicDBObject.parse("{pageCount: 1}"))
                .iterator();

        while (mongoCursor.hasNext()) {
            Document document = mongoCursor.next();
//            System.out.println(document.get("pageCount"));
            System.out.println(document);
        }
    }

    @Test
    public void createDocument() {
        MongoCollection<Document> books = getDatabase().getCollection("books");
//        BasicDBObject newBook = new BasicDBObject();
//        newBook.put("title", "Java 10");
//        newBook.put("pageCount", 250);

        Document newBook = new Document("title", "Java 11")
                .append("pageCount", 250);
        books.insertOne(newBook);

        FindIterable<Document> documents = books.find()
                .filter(Filters.eq("title", "Java 11"));

        for (Document document : documents) {
            System.out.println(document);
        }

    }

    private MongoDatabase getDatabase() {
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        return mongoClient.getDatabase("java20");

    }

}