package pl.sda.jp.nosql;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.function.Consumer;

public class App {

    public static void main(String[] args) {
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        MongoDatabase java20 = mongoClient.getDatabase("java20");

        java20.listCollectionNames()
                .forEach((Consumer<? super String>) System.out::println);

        MongoCollection<Document> books = java20.getCollection("books");

        MongoCursor<Document> mongoCursor = books.find().iterator();

        while (mongoCursor.hasNext()) {
            Document document = mongoCursor.next();
            System.out.println(document);
        }
    }
}
